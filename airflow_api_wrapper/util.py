from functools import partial

from google.auth.transport.requests import Request
from google.oauth2 import id_token
import requests

IAM_SCOPE = "https://www.googleapis.com/auth/iam"
OAUTH_TOKEN_URI = "https://www.googleapis.com/oauth2/v4/token"

## change with airflow/composer image

# Navigate to your webserver's login page and get this from the URL
# Or use the script found at
CLIENT_ID = "286994878426-i5t5t098bhklldb95uoqdmtq7nqp00f2.apps.googleusercontent.com"
# This should be part of your webserver's URL:
# {tenant-project-id}.appspot.com
# https://ra8518b0ee1ca3f09p-tp.appspot.com
WEBSERVER_ID = "ra8518b0ee1ca3f09p-tp"


def call_airflow_api(endpoint, method, data=None, context=None, **kwargs):
    """Makes a POST request to the Composer DAG Trigger API

    When called via Google Cloud Functions (GCF),
    data and context are Background function parameters.

    For more info, refer to
    https://cloud.google.com/functions/docs/writing/background#functions_background_parameters-python

    To call this function from a Python script, omit the ``context`` argument
    and pass in a non-null value for the ``data`` argument.
    [summary]

    Args:
        data: CF background parameters
        context: CF background parameters
        kwargs: user's pass in parameter for dag run
            json_data: the DAG's conf dict
    Returns:
        [type]: [description]

    """
    # Fill in with your Composer info here
    # Navigate to your webserver's login page and get this from the URL
    # Or use the script found at
    # https://github.com/GoogleCloudPlatform/python-docs-samples/blob/master/composer/rest/get_client_id.py
    # client_id = 'YOUR-CLIENT-ID'
    if endpoint[0] != "/":
        endpoint = "/" + endpoint
    endpoint = "/api/v1" + endpoint

    webserver_url = "https://" + WEBSERVER_ID + ".appspot.com" + endpoint

    # Make a POST request to IAP which then Triggers the DAG
    r = make_iap_request(webserver_url, CLIENT_ID, method=method, **kwargs)

    return r.json()


# This code is copied from
# https://github.com/GoogleCloudPlatform/python-docs-samples/blob/master/iap/make_iap_request.py
# START COPIED IAP CODE
def make_iap_request(url, client_id, method="GET", **kwargs):
    """Makes a request to an application protected by Identity-Aware Proxy.
    Args:
      url: The Identity-Aware Proxy-protected URL to fetch.
      client_id: The client ID used by Identity-Aware Proxy.
      method: The request method to use
              ('GET', 'OPTIONS', 'HEAD', 'POST', 'PUT', 'PATCH', 'DELETE')
      **kwargs: Any of the parameters defined for the request function:
                https://github.com/requests/requests/blob/master/requests/api.py
                If no timeout is provided, it is set to 90 by default.
    Returns:
      The page body, or raises an exception if the page couldn't be retrieved.
    """
    # Set the default timeout, if missing
    if "timeout" not in kwargs:
        kwargs["timeout"] = 90

    # Obtain an OpenID Connect (OIDC) token from metadata server or using service
    # account.
    google_open_id_connect_token = id_token.fetch_id_token(Request(), client_id)

    # Fetch the Identity-Aware Proxy-protected URL, including an
    # Authorization header containing "Bearer " followed by a
    # Google-issued OpenID Connect token for the service account.
    resp = requests.request(
        method,
        url,
        headers={
            "Authorization": "Bearer {}".format(google_open_id_connect_token),
            "Content-Type": "application/json",
        },
        **kwargs,
    )
    if resp.status_code == 403:
        raise Exception(
            "Service account does not have permission to "
            "access the IAP-protected application."
        )
    elif resp.status_code != 200:
        print(resp.status_code)
        print(resp.headers)
        print(resp.text)
        raise Exception("Bad response from application")
    else:
        return resp


if __name__ == "__main__":
    from rich import print

    dag_name = "test_send_grid"
    # endpoint = f"api/v1/dags/{dag_name}/dagRuns"
    endpoint = f"dags/{dag_name}/dagRuns"
    # endpoint = f"api/v1/dags/{dag_name}/tasks"
    # r = call_airflow_api(endpoint, "POST", json={"conf": {"a": 1}})
    r = call_airflow_api(endpoint, "POST", json={"conf": {"a": 1}})
    # r = call_airflow_api(endpoint, "POST", con={"a": 1})
    print(r)
    # r = dag_is_running(dag_id=r["dag_id"], dag_run_id=r["dag_run_id"])
    # print(r)

    # curl -X POST http://localhost:8080 \
    # -H "Content-Type: application/json" \
    # -d '{"method" :"POST", "endpoint":"dags/test_send_grid/dagRuns", "kwargs": {"conf": {"a":1}}}'
