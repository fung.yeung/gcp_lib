from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
from google.cloud import secretmanager


def send_email(error_config: str, error_msg: str):
    """send email on process crash

    Args:
        chat_event (str): chat_event JSON string for debugging purpose
        error (str): error context from python
    """
    message = Mail(
        from_email="chatbot@veolia.com",
        to_emails=["fung.yeung@veolia.com"],  # , "jean-marc.bonnefoy@veolia.com"]
        subject="[OCR & Translate Chatbot] Error encountered",
        html_content=f"""<h3>error message:</h3>
        <br>
        {error_msg}
        <br>
        <h3>Error config:
        <br>
        {error_config}
        </h3>

        """,
    )
    try:
        api_key = get_sendgrid_api_key()
        sg = SendGridAPIClient(api_key=api_key)
        response = sg.send(message)
    except Exception as e:
        print(str(e))


def get_sendgrid_api_key():
    name = f"projects/cn-ops-spdigital/secrets/sendgrid_api_key/versions/latest"
    client = secretmanager.SecretManagerServiceClient()
    response = client.access_secret_version(name=name)
    return response.payload.data.decode("UTF-8")


if __name__ == "__main__":
    send_email(1, 1)
