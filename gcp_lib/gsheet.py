import gspread
import gspread_dataframe as gd
import pandas as pd
import google.auth

from gspread.exceptions import GSpreadException


class GSheet:
    def __init__(self) -> None:
        credentials, _ = google.auth.default(
            scopes=[
                "https://spreadsheets.google.com/feeds",
                "https://www.googleapis.com/auth/drive",
            ]
        )
        self.gc = gspread.authorize(credentials)

    def get_as_sheet(
        self, spreadsheet_url_or_key: str, sheet_name: str
    ) -> gspread.Worksheet:
        """get google sheet as gspread worksheet object,
        usually it is easier to use "get_as_df()"

        Args:
            spreadsheet_url_or_key (str): [description]
            sheet_name (str): [description]

        Returns:
            gspread.Worksheet: [description]
        """
        if spreadsheet_url_or_key.startswith("https://docs.google.com/spreadsheets"):
            gc_open_method = getattr(self.gc, "open_by_url")
        else:
            gc_open_method = getattr(self.gc, "open_by_key")

        return gc_open_method(spreadsheet_url_or_key).worksheet(sheet_name)

    def get_as_df(self, spreadsheet_url_or_key: str, sheet_name: str) -> pd.DataFrame:
        """get google sheet as dataframe, also set cls attribute 'last_ws'
            for easy writing back to the same sheet

        Args:
            spreadsheet_url_or_key (str): [description]
            sheet_name (str): [description]

        Returns:
            pd.DataFrame: [description]
        """
        ws = self.get_as_sheet(spreadsheet_url_or_key, sheet_name)
        self.last_ws = ws
        try:
            return pd.DataFrame(ws.get_all_records())
        except GSpreadException:
            print("there is duplicate in header, header is now in first row")
            return pd.DataFrame(ws.get_all_values())

    def get_cell_value(self, ws, target_cell: str | list[str]):
        output = {}
        if isinstance(target_cell, str):
            target_cell = [target_cell]

        for tc in target_cell:
            output[tc] = ws.acell(tc).value

        return output

    def to_gsheet(
        self,
        df: pd.DataFrame,
        ws: gspread.Worksheet = None,
        append: bool = False,
        **kwargs
    ) -> None:
        """write dataframe to gsheet
        refer https://pythonhosted.org/gspread-dataframe/ for other parameter in write options

        Args:
            df (pd.DataFrame): [description]
            ws (gspread.Worksheet, optional): ws to write to, if None. Defaults to None.
            append (Boolean, optional): [description]. Defaults to False.
        """
        #
        if ws is None:
            try:
                ws = self.last_ws
            except AttributeError:
                print(
                    "There is no last read worksheet, need to specify a target worksheet!"
                )
                raise

        if not append:
            ws.clear()
            row = 1
            include_column_header = True
        else:
            row_count = len(ws.get_all_values())
            row = row_count + 1
            include_column_header = False

        gd.set_with_dataframe(
            worksheet=ws,
            dataframe=df,
            row=row,
            include_column_header=include_column_header,
            **kwargs
        )


if __name__ == "__main__":
    spreadsheet_id = "1-CErwAPkuS9Vi_-olJtB6GyxxqMSHs703y5JMqgblus"
    spreadsheet_id = "https://docs.google.com/spreadsheets/d/1-CErwAPkuS9Vi_-olJtB6GyxxqMSHs703y5JMqgblus/edit#gid=0"
    sheet_name = "data"

    gs = GSheet()
    ws = gs.get_as_sheet(spreadsheet_id, sheet_name)
    out = gs.get_cell_value(ws, ["A1", "B1"])
    print(out)
