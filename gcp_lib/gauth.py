
import google.auth
from google.auth import credentials
import gspread

SCOPES = ['https://www.googleapis.com/auth/drive',
              'https://www.googleapis.com/auth/spreadsheets']
credentials, project_id = google.auth.default(scopes=SCOPES)

print(dir(credentials))
print(credentials.id_token)
# gc = gspread.authorize(credentials)
# sht = gc.open_by_url('https://docs.google.com/spreadsheets/d/1pd8zUHZfDJbM461TPMUfEoCVE6FT0lhktoWwjr3bfPg/edit#gid=956748788')

# print(sht)