import requests

import google.auth
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from google.auth.transport.requests import Request


class GoogleDrive:
    def __init__(self):
        """

        Ref:
            https://googleapis.github.io/google-api-python-client/docs/dyn/docs_v1.html
        """

        self.drive = self._get_drive_client()
        self.permissions = self.drive.permissions

    def get_id_from_url(self, drive_url):
        return "https://drive.google.com/drive/folders/1bPexy9BtIL4UykZ6e8xRerpEs7lRttIR".split(
            "/"
        )[
            -1
        ]

    def _get_drive_client(self):
        """return a client to interact with google drives

        Returns:
            _type_: _description_
        """
        SCOPES = ["https://www.googleapis.com/auth/drive"]
        credentials, _ = google.auth.default(scopes=SCOPES)
        self.credentials = credentials
        drive = build("drive", "v3")  # , credentials=credentials)
        return drive

    def get_file(
        self,
        file_id,
        fields: str = "id, name, mimeType, modifiedTime",
        quiet: bool = False,
    ) -> bool:
        """return True if no issue getting to the file, otherwise False

        Args:
            file_id (_type_): _description_
            supress_error (bool, optional): _description_. Defaults to False.

        Returns:
            bool: _description_
        """
        try:
            return self.drive.files().get(fileId=file_id, fields=fields).execute()
        except HttpError:
            print(
                "file not found: can be permission for SA not granted OR wrong file_id"
            )
            if not quiet:
                raise
        return False

    def ls(
        self, folder_id: str = None, query_string: str = None, fields: str = None
    ) -> list:
        """list files in a folder

        Args:
            folder_id (str, optional): If folder_id is given, return files in
            the folder, otherwise return user's root dir. Defaults to None.

            query_string (str, optional): filter files to return using API
            parameter. Defaults to None.
            https://developers.google.com/drive/api/guides/search-files#examples

            fields (str, optional): fields of the files to return, the lesser
            the more performance. Default to return only id, name, mimeType.
            https://developers.google.com/drive/api/guides/fields-parameter


        Returns:
            _type_: _description_
        """
        if "http" in folder_id:
            folder_id = self.get_id_from_url(drive_url=folder_id)

        if folder_id:
            q = f"parents='{folder_id}'"
        else:
            q = query_string

        fields = fields or "files(id, name, mimeType)"

        result = (
            self.drive.files()
            .list(
                pageSize=None,  # maximum is 1000 records/files in a folder, which shouldn't be possible
                fields=fields,
                q=q,
                includeItemsFromAllDrives=True,
                supportsAllDrives=True,
            )
            .execute()
        )
        return result["files"]

    def grant_permission_to_user(
        self,
        file_id: str,
        user_email: str,
        email_message: str,
        transfer_ownership: bool,
        role: str = "reader",
    ):
        """grant a drive's file permission to user

        Args:
            file_id (str): _description_
            user_email (_type_): _description_
            body (dict): https://developers.google.com/drive/api/v3/reference/permissions/create#request-body
            parameter (dict): https://developers.google.com/drive/api/v3/reference/permissions/create#parameters
            role (str, optional): _description_. Defaults to "reader".

        Returns:
            _type_: _description_
        """

        # https://developers.google.com/drive/api/v3/reference/permissions/create#parameters
        body = {"type": "user", "role": role, "emailAddress": user_email}
        parameter = {
            "emailMessage": email_message,
            "transferOwnership": transfer_ownership,
        }

        return self.grant_permission(file_id=file_id, body=body, parameter=parameter)

    def download_file(self, file_id) -> bytes:
        """_summary_

        Returns:
            _type_: _description_
        """
        conversion_mapping = {
            # only apply to google workspace file, i.e. Gdoc, Gsheet, Gslide
            "application/vnd.google-apps.document": "text/plain",  # Gdoc as plain text file
            "application/vnd.google-apps.spreadsheet": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",  # gsheet as MS excel
            "application/vnd.google-apps.presentation": "application/vnd.openxmlformats-officedocument.presentationml.presentation",  # Gslide as MS powerpoint
        }

        if self.credentials.token is None:
            self.credentials.refresh(Request())

        # metadata = self.get_file(file_id=file_id, fields="id,name,mimeType")
        metadata = self.get_file(file_id=file_id, fields="id,name,mimeType,exportLinks")

        mime_type = metadata["mimeType"]
        if "google" not in mime_type:
            url = f"https://www.googleapis.com/drive/v3/files/{file_id}?alt=media"
        else:
            # google workspace mimeType
            export_file_format = conversion_mapping[mime_type]
            url = metadata["exportLinks"][export_file_format]
            # url = f"https://docs.google.com/spreadsheets/export?id={file_id}&exportFormat=xlsx"

        headers = {"Authorization": "Bearer " + self.credentials.token}
        r = requests.get(url, headers=headers)
        r.raise_for_status()
        return r.content

    def grant_permission(self, file_id: str, body: dict, parameter: dict):
        """grant permission on a file_id, since there is a lot combination and parameter setting for type/role combination, function caller can pass in the parameter it needs ni a dict.

        Args:
            file_id (str): _description_
            type (str, optional): _description_. Defaults to "user".
            role (str, optional): _description_. Defaults to "reader".

        Ref:
            https://developers.google.com/resources/api-libraries/documentation/drive/v3/python/latest/drive_v3.permissions.html#create
        """

        self.permissions().create(fileId=file_id, body=body, **parameter).execute()

    def mv(self, file_id, dest_folder):

        gdrive_prefix = "https://drive.google.com/drive/folders/"
        dest_folder_id = dest_folder.replace(gdrive_prefix, "")

        previous_parents = self.get_file(file_id=file_id, fields="parents")["parents"][
            0
        ]
        self.drive.files().update(
            fileId=file_id,
            addParents=dest_folder_id,
            removeParents=previous_parents,
            fields="id, parents",
            supportsAllDrives=True,
        ).execute()


if __name__ == "__main__":
    from rich import print

    d = GoogleDrive()
    file_ids = [
        "1lz9GZ4mwtCyiLucjNzPb3xrwyXJFFdQdUSVFXafo4XM",
        "1rEaYjn_NMF8Sw8KhBBRl9aAHCWm2LMYO5Yv8L0c4tFE",
        "1B2ZVHGuF6dtu3MSQRJ3YNw9eFIlchalxW_Ls3IMkylk",
        "1KJPPoQIMltcICHtYMQT-d_CN-Jh9qRehuEhkQW3JWoo",
        "1xy9CDOTB3n75CzwWyuj6wnPHLlb7hDRhgozipE2X-VI",
        "1s1qBAif0Q0swI5nLg-NcdDAGUlDIJ5u8jjCEsx4Wezw",
    ]
    for file_id in file_ids:
        f = d.get_file(file_id=file_id)
        print(f)
    # output = d.ls(
    #     "1s3INwBluRrCLRpUXOxXaF6wsoZwrv6dH",
    #     # query_string="mimeType='application/vnd.google-apps.folder'",
    # )
    # file_id = "14lfGN8Z8yTEU3uFNXpieoTVO8nC0rBBZfCfYKirEaEg"
    # src_folder = (
    #     "https://drive.google.com/drive/folders/1bPexy9BtIL4UykZ6e8xRerpEs7lRttIR"
    # )
    # dest_folder = (
    #     "https://drive.google.com/drive/folders/1DhmOsu5yrkGUZKrlsrbrqh8ku90T34uq"
    # )
    # d.mv(file_id=file_id, dest_folder=dest_folder)
    # print(d.get_file(file_id=file_id, fields="parents"))
    # d.mv(file_id=file_id, src_folder=src_folder, dest_folder=dest_folder)
    # # q = "'1s3INwBluRrCLRpUXOxXaF6wsoZwrv6dH' in parents'"
    # results = (
    #     d.drive.files()
    #     .list(
    #         q="'1s3INwBluRrCLRpUXOxXaF6wsoZwrv6dH' in parents and name='"
    #         + "vams-asia-rawdata-ingestion@cn-ops-spdigital-dev-dev.iam.gserviceaccount.com.json"
    #         + "'",
    #         pageSize=100,
    #         fields="nextPageToken, files(id, name)",
    #         corpora="drive",
    #         driveId="0ADuyPny85kqCUk9PVA",
    #         supportsAllDrives=True,
    #         includeItemsFromAllDrives=True,
    #     )
    #     .execute()
    # )
    # print(results)
    # # fn = "vams-asia-rawdata-ingestion@cn-ops-spdigital-dev-dev.iam.gserviceaccount.com.json"
    # q = f"parents='1s3INwBluRrCLRpUXOxXaF6wsoZwrv6dH' and name = '{fn}'"

    # results = (
    #     d.drive.files()
    #     .list(
    #         q=q,
    #         pageSize=100,
    #         fields="files(id, name)",
    #         # corpora="drive",
    #         # driveId="ttt",
    #         supportsAllDrives=True,
    #         includeItemsFromAllDrives=True,
    #     )
    #     .execute()
    # )
    # print(results)

    # print(output)
    # 'application/vnd.google-apps.folder'
    # output = d.get_file("13fSiVgblyP_tLjrgPTetRISlV_l-Z9Ml", quiet=True)
    # # print(output)
    # permission = {
    #     "type": "user",
    #     "role": "writer",
    #     "emailAddress": "specialprojects.asi.int.tec@veolia.com",
    #     "transferOwnership": True,
    # }
    # for f in d.ls(folder_id="13fSiVgblyP_tLjrgPTetRISlV_l-Z9Ml"):
    #     fn = f["name"]
    #     d.grant_permission_to_user(
    #         file_id=f["id"],
    #         user_email="fung.yeung@veolia.com",
    #         email_message="How are you today?",
    #         transfer_ownership=True,
    #         role="editor",
    #     )
    #     break
    #     # if fn == "main.py":

    #     break
