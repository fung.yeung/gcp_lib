# Imports the Cloud Logging client library
from datetime import datetime

import google.cloud.logging
import logging


# logging.basicConfig(
#     filename="abc.txt",
#     format="%(asctime)s %(levelname)-8s %(message)s",
#     level=logging.INFO,
#     datefmt="%Y-%m-%d %H:%M:%S",
# )
# Instantiates a client
logging_client = google.cloud.logging.Client(project="cn-ops-spdigital")

for m in logging_client.list_metrics():
    print()

# logger = logging_client.logger(__name__)

# logger.log
# # Retrieves a Cloud Logging handler based on the environment
# # you're running in and integrates the handler with the
# # Python logging module. By default this captures all logs
# # at INFO level and higher
# # logger.setup_logging()

# text = "fung_test"

# data_dict = {"hello": "world"}
# logger.("tstfung_123", extra={"json_fields": data_dict})
# logger.warning("testfung2_234")
