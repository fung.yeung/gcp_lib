import subprocess
from typing import Tuple

# from distutils.command.clean import clean
from typing import Union, Dict, Literal
from google.cloud.bigquery.table import Table

import pandas as pd

from pprint import pprint
from google.cloud import bigquery
from google.cloud.bigquery.routine import Routine
from google.cloud.bigquery import dataset

from google.api_core.exceptions import NotFound


class BigQuery:
    def __init__(self, project_id: str = None, dataset_id: str = None) -> None:
        """
        Args:
            project_id (str, optional): target specific project, otherwise use gcloud SDK default project. Defaults to None.
            dataset_id (str, optional): target specific dataset_id. Defaults to None.

            https://googleapis.dev/python/bigquery/latest/generated/google.cloud.bigquery.client.Client.html#google.cloud.bigquery.client.Client
        """
        self.client = bigquery.Client(project_id)
        if dataset_id is not None:
            self.dataset_id = dataset_id

    def exec_query(
        self, query: str, as_df: bool = False, **kwargs
    ) -> Union[bigquery.job.QueryJob, pd.DataFrame]:
        # TODO: add GB check
        """query bigquery

        Args:
            query (str): [description]
            as_df (bool, optional): [description]. Defaults to False.
            **kwargs: optionals arguments to be used for as_dataframe()
            https://googleapis.dev/python/bigquery/latest/generated/google.cloud.bigquery.job.QueryJob.html#google.cloud.bigquery.job.QueryJob.to_dataframe

        Returns:
            Union[bigquery.job.QueryJob, pd.DataFrame]
        """
        output = self.client.query(query)
        if as_df:
            output = output.to_dataframe(**kwargs)
        return output

    def get_table_ddl(self, table) -> str:
        _, dataset_id, table_id = table.split(".")

        query = f"""
            SELECT
                ddl
            FROM
                `{dataset_id}`.INFORMATION_SCHEMA.TABLES
            WHERE
                TABLE_NAME = '{table_id}'        
        """
        job = self.exec_query(query)
        result = job.result()
        return next(job.result())[0]
        # return result[0]

    def get_table(self, table_name):
        return self.client.get_table(table=table_name)

    def list_datasets(
        self, raw: bool = False, **kwargs
    ) -> Union[list, dataset.DatasetListItem]:
        """list dataset in project.

        Args:
            raw (bool, optional): [description]. Defaults to False.

        Returns:
            Union[list, DatasetListItem]: [description]
        """
        if raw:
            return self.client.list_datasets(**kwargs)
        return [ds.dataset_id for ds in self.client.list_datasets(**kwargs)]

    def get_table(self, table):
        return self.client.get_table(table)

    def get_schema(self, table, format=None | Literal["sql", "sink_schema"]):
        """get table's schema. default format as python dict

        Args:
            table ([type]): [description]
            sink_schema_format (bool, optional): display result as logsheet
            ingestion connector sink_schema format.
            raw (bool, optional): [description]. Defaults to False.

        Returns:
            [type]: [description]
        """
        output = None
        raw_schema = self.client.get_table(table).schema

        if format == "sink_schema":
            return {col.name: col.field_type for col in raw_schema}
        elif format == "sql":
            return ", \n".join([f"{col.name} {col.field_type}" for col in raw_schema])
        else:
            output = [s.to_api_repr() for s in raw_schema]

        return output

    def get_columns(self, table: str) -> list:
        return [col["name"] for col in self.get_schema(table)]

    def update_schema(self, table: str, new_schema: list[dict]):
        """update schema of a table

        Args:
            table (str): target table
            new_schema (list[dict]): BigQuery's SchemaField Object in python
            dict format. Everything is addictive, if present it will change to
            new value, if it isn't then it won't be changed. If want to remove
            a field, just provide with value 'None'.

            SchemaField object: https://googleapis.dev/python/bigquery/latest/generated/google.cloud.bigquery.schema.SchemaField.html#google.cloud.bigquery.schema.SchemaField

        Example:
            in new_schema {'name': <col name>, '<key you want to add, e.g.
            description>: <value>'}.

        Returns:
            [type]: [description]
        """
        schema = self.get_schema(table)
        # create a list position dict for updating individual SchemaField in the original schema list
        # idx_mapping = {field["name"]: idx for idx, field in enumerate(schema)}

        # for field in new_schema:
        #     field_pos = idx_mapping[field["name"]]
        #     schema[field_pos].update(field)

        t = self.get_table(table)
        t.schema = new_schema
        return self.client.update_table(t, ["schema"])

    def peek_table(self, table, max_results=5, raw: bool = False, **kwargs):
        """UI's PREVIEW tab

        Args:
            table ([type]): [description]
            max_results (int, optional): [description]. Defaults to 5.
            raw (bool, optional): [description]. Defaults to False.

        Returns:
            [type]: [description]
        """
        return self.client.list_rows(table, max_results=max_results, **kwargs)

    def list_tables(self, dataset, raw: bool = False, **kwargs):
        if raw:
            return self.client.list_tables(dataset=dataset, **kwargs)
        return [
            tbl.table_id for tbl in self.client.list_tables(dataset=dataset, **kwargs)
        ]

    def delete_table(
        self, table: str, not_found_ok: bool = True, quiet: bool = False
    ) -> None:
        """delete a table.

        Args:
            table (str): target table to delete
            not_found_ok (bool): surpress error if table doesn't exist
            quiet (bool): whether to surpress message
        """

        not_found_flag = False
        try:
            self.client.delete_table(table, not_found_ok=False)
        except NotFound:
            not_found_flag = True
            if not not_found_ok:
                raise

        if not quiet:
            if not_found_flag:
                print(f'"{table}" does not exist!')
            else:
                print(f'deleted "{table}"')

    def load_table_from_dataframe(self, df, destination, **kwarg):
        return self.client.load_table_from_dataframe(
            df, destination=destination, **kwarg
        )

    def get_dataset(self, dataset: str):
        """get dataset object, mostly use to see it attributes"""
        return self.client.get_dataset(dataset)

    def create_gsheet_external_tbl(
        self,
        gsheet_url: str,
        gsheet_tab_name: str,
        full_table_id: str,
        schema: list[Tuple],
        data_row_start: int,
    ):
        """_summary_

        Args:
            gsheet_url (str): _description_
            gsheet_tab_name (str): _description_
            full_table_id (str): _description_
            schema (list[tuple): list of tuple(<col_name>, <col_type>)
            data_row_start (int): _description_
        """
        client = bigquery.Client()
        project, dataset_id, table_id = full_table_id.split(".")

        # Configure the external data source
        dataset_ref = bigquery.DatasetReference(project, dataset_id)
        schema = [SchemaField(col_name, col_type) for col_name, col_type in schema]

        table = bigquery.Table(dataset_ref.table(table_id), schema=schema)

        # set external config
        external_config = bigquery.ExternalConfig("GOOGLE_SHEETS")
        external_config.source_uris = [gsheet_url]
        external_config.options.skip_leading_rows = (
            data_row_start  # optionally skip header row
        )
        external_config.options.range = gsheet_tab_name
        table.external_data_configuration = external_config

        client.delete_table(table, not_found_ok=True)
        table = client.create_table(table)

    def grant_table_access(
        self,
        project_id: str,
        dataset_id: str,
        table_id: str,
        bq_role: str,
        bq_member: str,
    ):
        "https://cloud.google.com/sdk/gcloud/reference/projects/add-iam-policy-binding"

        bq_cmd_str = f"bq add-iam-policy-binding --member={bq_member} --role={bq_role} {project_id}:{dataset_id}.{table_id}"
        bq_cmd = bq_cmd_str.split(" ")
        res = subprocess.run(bq_cmd, stdout=subprocess.PIPE)
        print(res.stdout.decode("utf-8"))

    def grant_dataset_access(
        self,
        dataset: str,
        role: str,
        entity_type: str,
        entity_id: Union[str, Dict[str, str]],
    ) -> dataset.Dataset:
        """Grant dataset assess to other users

        Args:
            dataset (str): <project_id>.<dataset_name>
            role (str): ‘READER’, ‘WRITER’, ‘OWNER’
            entity_type (str): 'domain', 'groupByEmail', 'iamMember', 'routine', 'specialGroup', 'userByEmail', 'view'
            entity_id (Union[str, Dict[str, str]]): email string if entity isn't routine or view, see doc for view or routine.

        Returns:
            [type]: [description]

        https://googleapis.dev/python/bigquery/latest/generated/google.cloud.bigquery.dataset.AccessEntry.html
        """
        dataset = self.client.get_dataset(dataset)  # Make an API request.

        entry = bigquery.AccessEntry(
            role=role, entity_type=entity_type, entity_id=entity_id
        )

        entries = list(dataset.access_entries)
        entries.append(entry)
        dataset.access_entries = entries

        dataset = self.client.update_dataset(
            dataset, ["access_entries"]
        )  # Make an API request.

        full_dataset_id = "{}.{}".format(dataset.project, dataset.dataset_id)
        print(f"Granted access '{full_dataset_id}' to {entity_id}")
        return dataset


if __name__ == "__main__":
    # TODO: add GB check
    # Construct a BigQuery client object.
    # from rich import print
    import pyperclip
    from rich import print

    bq = BigQuery()
    c = {
        "project_id": "cn-ops-spdigital-dev-dev",
        "dataset_id": "plastiloop_monitoring",
        "table_id": "ref_entity",
        "bq_role": "roles/bigquery.dataViewer",
        "bq_member": "domain:veolia.com",
    }
    bq.grant_table_access(**c)
    # project_id = "gbl-imt-ve-datalake-demo"
    # project_id = "in-ops-india-operationdb"
    # dataset_id = "projects_data"
    # bq = BigQuery(project_id=project_id, dataset_id=dataset_id)

    # pyperclip.copy(output)
    # pyperclip.copy(r)
    pass
    # def create_external_tbl(file_name, gcs_file_date, cols_with_type: str = None):
    #     if file_name.lower().endswith("v1"):
    #         file_name = file_name.replace("V_", "").replace("_v1", "")

    #     if cols_with_type is None:
    #         sql = f"""CREATE or replace EXTERNAL TABLE
    #             cn-ops-spdigital-dev-dev.tests.gcs_{file_name}
    #         options (
    #             format=JSON,
    #             uris=['gs://00931-vams-asia-demo/__outputs/eam_V_{file_name}_{gcs_file_date}*.json']
    #         );
    #         """

    #     else:

    #         sql = f"""CREATE or replace EXTERNAL TABLE
    #             cn-ops-spdigital-dev-dev.tests.gcs_{file_name} (
    #                 {cols_with_type}
    #             )
    #         options (
    #             format=JSON,
    #             uris=['gs://00931-vams-asia-demo/__outputs/eam_V_{file_name}_{gcs_file_date}*.json']);
    #             """
    #     bq.exec_query(sql)

    #     return bq.get_columns(f"cn-ops-spdigital-dev-dev.tests.gcs_{file_name}")

    # def get_external_tbl_cols_type(file_name, ext_cols):
    #     """fetch 931 col type to be use in create extenral table, if cols not
    #     exist in 931 would be cast to string (won't be use downstream anyway)

    #     Args:
    #         file_name (_type_): _description_
    #         current_ext_cols (_type_): _description_
    #     """
    #     with open("931_tbl_mapping.json") as f:
    #         mapping = json.load(f)

    #     cols_931 = mapping[file_name]["col"]
    #     cols_931_mapping = {
    #         c["name"]: (c["type"]).replace("FLOAT", "FLOAT64") for c in cols_931
    #     }

    #     return ", ".join([f'{c} {cols_931_mapping.get(c, "STRING")}' for c in ext_cols])

    #     # cols_931_name_only = [c["name"] for c in cols_931]
    #     # common_cols = [c for c in cols_931_name_only if c in ext_cols]
    #     # common_cols_sql = ", \n".join(common_cols)
    #     # cols = ", \n".join(
    #     #     [
    #     #         f"cast ({c} as {cols_931_mapping[c]}) as {c}"
    #     #         for c in common_cols
    #     #         if c in cols_931_name_only
    #     #     ]
    #     # )

    # def get_compare_sql(file_name: str, ext_cols: list):
    #     """both 931 and gcs json file can have different unique columns,
    #     the algorithem is to:
    #         1. get common cols in GCS AND BQ
    #         2. cast GCS external table dataype using BQ datatype
    #         3. return compare SQL for execution

    #     Args:
    #         file_name (str): _description_
    #         ext_cols (list): _description_

    #     Returns:
    #         _type_: _description_
    #     """
    #     with open("931_tbl_mapping.json") as f:
    #         mapping = json.load(f)

    #     cols_931 = mapping[file_name]["col"]
    #     cols_931_mapping = {
    #         c["name"]: (c["type"]).replace("FLOAT", "FLOAT64") for c in cols_931
    #     }
    #     cols_931_name_only = [c["name"] for c in cols_931]
    #     common_cols = [c for c in cols_931_name_only if c in ext_cols]
    #     common_cols_sql = ", \n".join(common_cols)
    #     cols = ", \n".join(
    #         [
    #             f"cast ({c} as {cols_931_mapping[c]}) as {c}"
    #             for c in common_cols
    #             if c in cols_931_name_only
    #         ]
    #     )

    #     sql = f"""
    #         select
    #             {cols}
    #         FROM
    #             `cn-ops-spdigital-dev-dev.tests.gcs_{file_name}`
    #         EXCEPT distinct
    #         select
    #             {common_cols_sql}
    #         from
    #             `gbl-imt-ve-datalake-demo.00931_vams_raw_asia.V_{file_name}_v1`
    #     """
    #     return sql

    # fn = "R5ACTCHECKLISTS"
    # ext_tbl_cols = create_external_tbl(file_name=fn, gcs_file_date="2022-09-13")

    # # sql = get_compare_sql(file_name=fn, ext_cols=ext_tbl_cols)
    # # print(sql)

    # print(get_external_tbl_cols_type(fn, ext_tbl_cols))
    # with open("931_tbl_mapping.json") as f:
    #     mapping = json.load(f)

    # # for k, v in mapping.items():
    # # print(k, v["col"])
    # # target = "R5ACTCHECKLISTS"
    # # print(
    # #     get_create_external_tbl_sql(
    # #         file_name=target,
    # #         # gcs_file_date="2022-09-13",
    # #         cols_931=mapping[target]["col"],
    # #     )
    # # )

    # from collections import defaultdict

    # new_output = defaultdict(dict)
    # with open("sql_dict.json") as f:
    #     compare_sql = json.load(f)
    #     for k, v in compare_sql.items():
    #         new_output[k]["compare"] = v
    #         new_output[k]["external_table"] = get_create_external_tbl_sql(
    #             k, mapping[k]["col"]
    #         )
    #         # print(get_create_external_tbl_sql(k, mapping[k]["col"]))

    # # output = {}
    # # for filename in mapping.keys():
    # #     output[filename] = get_compare_sql(filename)

    # # from pprint import pprint

    # with open("final.json", "w") as f:
    #     json.dump(new_output, f, indent=2)

    # with open("sql_dict.json") as f:
    #     d = json.load(f)
    #     print(d["R5USERS"])
    # print(get_compare_sql("R5USERORGANIZATION"))
    # with open("931_tbl_mapping.json", "w") as f:
    #     json.dump(output, f, indent=2)
    # schema = bq.get_schema("cn-ops-spdigital.tests.BOT_DataRaw_TEST_FUNG", format="sql")

    # for s in schema:
    #     s['name'] = s['name'].upper()

    # print(schema)
    # print(",\n".join(k for k in schema))
    # bq.client.delete_dataset("operation", delete_contents=True)
    # print(bq.client.create_dataset("operation"))
    # print("done")
    #   ['assets', 'work_orders']
    # bq.delete_table("tests.assets")

    # s = bq.get_schema("maintenance.workorder")
    # cols = bq.get_columns("00035_hubgrade_asia.data_asia_raw")
    # for col in s:
    #     # print(", " + col["name"] + " " + col["type"])
    #     # print(", CAST(" + col["name"] + " AS " + col["type"] + ")")
    #     print()
    # ddl = bq.get_table_ddl("cn-ops-spdigital.maintenance.workactivity")
    # print(ddl)
    # pyperclip.copy(ddl)
    # test new
    # print(", \n".join(cols))
    # tbls = bq.list_tables("operation")
    # this is news
    # print(bq.client.dataset)
    # print(bq.client.dataset_id)
    # for tbl in [
    #     "factor_bak_20210913",
    #     "factor_indicator_value_hist_ingest",
    #     "factor_indicator_value_old",
    #     "v_factor_indicator_value_old",
    # ]:
    #     bq.delete_table(f"cn-ops-spdigital.operation.{tbl}")
    #     break
    # print(ds)
    # tbls = [t for t in bq.list_tables("tests") if "test" in t]  # , raw=True)
    # print(*tbls, sep="\n")
    # bq.delete_table("tests.test_import", not_found_ok=True)
    pass
