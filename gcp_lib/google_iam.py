import subprocess


def get_service_account_access_token(sa_email: str):
    """_summary_

        ref: https://cloud.google.com/iam/docs/create-short-lived-credentials-direct#create-access
    Args:
        sa_email (str): _description_


    """
    cmd = f"gcloud auth print-access-token --impersonate-service-account={sa_email}"
    result = subprocess.run(cmd.split(), capture_output=True)
    return result.stdout.decode("utf-8").strip()


if __name__ == "__main__":
    sa_email = "dbt-runner@cn-ops-spdigital.iam.gserviceaccount.com"
    token = get_service_account_access_token(
        sa_email=sa_email,
    )
    print(token)
