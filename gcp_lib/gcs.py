from datetime import datetime, timezone
import re
from concurrent.futures import ThreadPoolExecutor

from google.cloud import storage
from google.cloud.storage import Blob
from google.api_core.page_iterator import HTTPIterator

# from datetime import datetime
# from multiprocessing import Pool


class CloudStorage:
    def __init__(self, project, bucket=None):
        self.client = storage.Client(project=project)
        self.bucket = bucket
        self.gcs_path_matcher = re.compile(pattern=r"(gs://)?(.*?)/(.*)")
        # self.bucket_pattern = "(gs://)?(.*?)/"

    def ls(self, prefix: str = None) -> HTTPIterator:
        # bucket = bucket or self.bucket

        # if prefix not provided, list bucket
        if prefix is None:
            return self.client.list_buckets()

        if prefix.startswith("gs://"):
            prefix = prefix.replace("gs://", "")

        splitted_prefix = prefix.split("/")
        if len(splitted_prefix) > 1:
            bucket, fn_prefix = prefix.split("/")
        else:
            bucket, fn_prefix = splitted_prefix[0], None

        return self.client.list_blobs(bucket_or_name=bucket, prefix=fn_prefix)

    def rm(self, blobs: HTTPIterator):
        def _remove_blob():
            for b in blobs:
                b.delete()
                print(f"deleted - {b.name}")

        # _remove_blob()
        with ThreadPoolExecutor(max_workers=32) as executor:
            future = executor.submit(_remove_blob)
            print(future.result())

    def put(self):
        self.client.bucket(bucket_name=bucket_name)
if __name__ == "__main__":
    s = "gs://00504-vams-asia/2021/12/01/EAM_V_R5BOOKEDHOURS_"
    # print('/'.join(s.split("/")[3:])
    from datetime import datetime, timezone

    a_month_ago = datetime(2022, 7, 21, tzinfo=timezone.utc)

    storage = CloudStorage(project="cn-ops-spdigital")
    prefix = "gs://asia-northeast1-pd-2021-oct-978d8d7a-bucket/logs/octopus_sync_bucket"
    blobs = storage.ls(prefix=prefix)
    blobs_removal = []
    for b in blobs:
        time_created = b.time_created
        if time_created > a_month_ago:
            action = "keep"
        else:
            action = "delete"
            blobs_removal.append(b)

    storage.rm(blobs_removal)
    # gcs = storage.Client(project="gbl-imt-ve-datalake-prod")
    # blobs = gcs.list_blobs("00504-vams-asia", prefix="2021/12/01/EAM_V_R5BOOKEDHOURS_")

    # full_gcs_path_blobs = [f"gs://{b.bucket.name}/{b.name}" for b in blobs]

    # def check_float_exist(gcs_path):
    #     # pd.read_json()
    #     # print(type(b.bucket))
    #     contains_float = False
    #     # gcs_path = f"gs://{b.bucket.name}/{b.name}"
    #     # print(gcs_path)
    #     print(f"reading {gcs_path}")
    #     df = pd.read_json(gcs_path, lines=True)
    #     if df.dtypes["BOO_HOURS"] != "int64":
    #         contains_float = True
    #     print(f"{gcs_path}-{contains_float}")
    #     return contains_float, gcs_path

    # def main():

    #     with Pool(processes=8) as p:  # or whatever your hardware can support
    #         tuples = p.map(check_float_exist, full_gcs_path_blobs)

    #     print(tuples)

    # main()
    # # print(tuples)
    # # break
    # # print(b.bucket.name)
    # # print(b.name)
    # # print(b.bucket)
    # # print(b.path)
    # # df = pd.read_csv
