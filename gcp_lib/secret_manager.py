import json
from pprint import pprint

import yaml
from google.cloud import secretmanager


class SecretManager:
    # https://googleapis.dev/python/secretmanager/latest/secretmanager_v1/secret_manager_service.html

    def access_secret_from_yaml(
        self, yaml_path: str, target_service_account: str = None
    ) -> dict:
        """get secret from YAML file that contains service-acc:secret info mapping

        Args:
            yaml_path (str): path to yaml file
            target_service_account (str, optional): target specfic service
            account if yaml file has multiple sa provided. Defaults to first
            service acocunt in the file.

        Returns:
            dict: secret content
        """

        with open(yaml_path, "r") as f:
            doc = yaml.safe_load(f)

        if target_service_account is None:
            target_service_account = next(iter(doc))

        try:
            sa_detail = doc[target_service_account]
        except KeyError:
            print(f"input service account {target_service_account} does not exist!")
            raise

        return self.access_secret(**sa_detail)

    def access_secret(self, project_id: str, secret_id: str, version: str) -> dict:
        """get secret from Secret Manager by providing raw components

        Args:
            project_id (str): project name
            secret_id (str): secret name
            version (str): user's defined version string when secret was created

        Returns:
            dict: secret content
        """

        client = secretmanager.SecretManagerServiceClient()
        name = f"projects/{project_id}/secrets/{secret_id}/versions/{version}"
        response = client.access_secret_version(name=name)
        payload = response.payload.data.decode("UTF-8")
        return json.loads(payload)


if __name__ == "__main__":
    a = json.dumps(SecretManager().access_secret("cn-ops-spdigital", "composer", "1"))
    pprint(a)
    # a = SecretManager().access_secret_from_yaml(
    #     "sa_mapping.yaml", "test_fung@cn-ops-spdigital.iam.gserviceaccount.com"
    # )
    # print(a)
