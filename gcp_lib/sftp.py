import paramiko


class SFTP:
    """
    Easy access for SFTP server, usage:

    doc:
    https://docs.paramiko.org/en/stable/api/sftp.html#paramiko.sftp_client.SFTPClient"""

    def __init__(self, credentials: dict[str, str]):
        """

        Args:
            credentials (dict[str, str]): expects host, username, password, port(optional)
        """
        self.credentials = credentials

    def __enter__(self) -> paramiko.SFTPClient:
        # ttysetattr etc goes here before opening and returning the file object
        transport = paramiko.Transport(
            sock=(credentials["host"], credentials.get("port", 22))
        )
        transport.connect(
            username=credentials["username"], password=credentials["password"]
        )
        self.sftp = paramiko.SFTPClient.from_transport(transport)
        return self.sftp

    def __exit__(self, type, value, traceback):
        self.sftp.close()

    def get(self, remotepath: str, localpath: str | None = None):
        self.sftp.get(remotepath=remotepath, localpath=localpath)

    def put(self, remotepath: str, localpath: str | None = None):
        self.sftp.put(remotepath=remotepath, localpath=localpath)

    def ls(
        self, remotepath: str, with_attr: bool = False
    ) -> list[str] | paramiko.SFTPAttributes:
        if with_attr:
            return self.sftp.listdir_attr(path=remotepath)
        else:
            return self.sftp.listdir(path=remotepath)


if __name__ == "__main__":
    from rich import print

    credentials = {
        "host": "s-94284420a37341589.server.transfer.eu-west-1.amazonaws.com",
        "username": "hubgrade-hongkong",
        "password": "0.zKiW?F:-.7kdJD",
    }

    sftp = SFTP(credentials=credentials)

    with sftp:
        # sftp.sftp.mkdir("/input")
        print(sftp.ls("."))
        print(sftp.ls("./input/"))
        # sftp.put(remotepath="./input/test_fung.txt", localpath="test_fung.txt")
