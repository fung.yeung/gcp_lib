import google.auth
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

from google.auth.transport import requests as google_auth_requests

import requests


class GoogleChat:
    def __init__(self, default_space_name: str = None):
        """init a client, all method would be target to default_space_name, can be overriden in individual method

        Args:
            space_name (str, optional): chat space name. Defaults to None.
        """
        self.client = self._get_client()
        self.space_name = default_space_name

    def _get_client(self):
        """return a client to interact with google drives

        Returns:
            _type_: _description_
        """
        SCOPES = [
            "https://www.googleapis.com/auth/chat.bot",
            # "https://www.googleapis.com/auth/chat.memberships.app",
            # "https://www.googleapis.com/auth/chat.memberships",
            # "https://www.googleapis.com/auth/chat.spaces.create",
        ]
        credentials, _ = google.auth.default(scopes=SCOPES)
        self.credentials = credentials
        return build("chat", "v1", credentials=credentials)

    def send_text_message(self, text: str, space_name: str = None):
        """send plain text message via Chat, space_name

        Args:
            text (str): _description_
            space_name (str, optional): _description_. Defaults to None.

        Returns:
            _type_: _description_
        """
        parent = space_name or self.space_name

        return (
            self.client.spaces()
            .messages()
            .create(parent=parent, body={"text": text})
            .execute()
        )

    def download_attachment(
        self, attachment_resource_name, attachment_content_type
    ) -> str | bytes:
        self.credentials.refresh(google_auth_requests.Request())
        token = self.credentials.token

        headers = {"Authorization": f"Bearer {token}"}
        url = (
            "https://chat.googleapis.com/v1/media/"
            + attachment_resource_name
            + "?alt=media"
        )

        res = requests.get(url=url, headers=headers)

        if attachment_content_type == "text/plain":
            output = res.text  # plain text
        else:
            output = res.content  # image / PDF

        print("fetched chat attachment")
        return output


if __name__ == "__main__":
    import os

    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "key/sa_key.json"

    # space = "spaces/nf5X34AAAAE"
    gc = GoogleChat(default_space_name="spaces/nf5X34AAAAE")
    output = gc.download_attachment(
        attachment_resource_name="ClxzcGFjZXMvbmY1WDM0QUFBQUUvbWVzc2FnZXMvSlZmY05MQ3hiaDAuSlZmY05MQ3hiaDAvYXR0YWNobWVudHMvQUFUVWYtTE9hQkRsSXB5bWkxUEc3dTVQR3hsUw==",
        attachment_content_type="application/pdf",
    )
    print(output)

    # header = {
    #     "header": {
    #         "title": "test Fung",
    #         "subtitle": "Card header",
    #         "imageUrl": "https://goo.gl/5obRKj",
    #         "imageStyle": "IMAGE",
    #     }
    # }

    # cards = list()
    # widgets = list()

    # (
    #     gc.client.spaces()
    #     .messages()
    #     .create(parent=space, body={"cards": [card]})
    #     .execute()
    # )
