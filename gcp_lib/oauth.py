import os
from pathlib import Path

import google.auth
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow


class OAuth:
    def __init__(self):
        self.adc_path = (
            Path(r"%APPDATA%\gcloud\application_default_credentials.json")
            if self._is_windows()
            else (Path.home() / ".config/gcloud/application_default_credentials.json")
        )

    def get_credential_obj(
        self,
        scopes: list[str],
        user_credential_filepath: str | None = None,
        service_account_file_path: str | None = None,
    ) -> Credentials:

        # check if user has either
        # 1. gcloud ADC credential on PC, mainly for developer (use it directly)
        # 2. user credential JSON filepath provided (click on authenticate page)

        if user_credential_filepath:
            credential = self._get_user_credential(
                user_credential_json_path=user_credential_filepath, oauth_scopes=scopes
            )

        elif self.adc_path.exists():
            credential = self._get_user_credential(
                token_path=str(self.adc_path),
                oauth_scopes=scopes,
            )

        elif service_account_file_path:
            credential, _ = google.auth.default(scopes=scopes)

        else:
            raise ValueError("Cannot fetch any credential")

        return credential

    def _is_windows(self):
        return True if os.name == "nt" else False

    def _get_user_credential(
        self,
        oauth_scopes: list,
        user_credential_json_path: str | None = None,
        oauth_local_port: int = 1234,
        token_path: str | None = None,
    ) -> Credentials:
        """https://developers.google.com/identity/protocols/oauth2/scopes"""
        creds = None
        # The file token.json stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if token_path is None:
            Path(".oauth").mkdir(exist_ok=True)
            token_path = ".oauth/token.json"

        if Path(token_path).exists():
            creds = Credentials.from_authorized_user_file(
                filename=token_path, scopes=oauth_scopes
            )

        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    user_credential_json_path, oauth_scopes
                )
                creds = flow.run_local_server(port=oauth_local_port)
            # Save the credentials for the next run
            user_credential_json_path = ".oauth/token.json"

            with open(user_credential_json_path, "w") as token:
                token.write(creds.to_json())
        return creds


o = OAuth()
cred = o.get_credential_obj(
    scopes=["https://www.googleapis.com/auth/drive"],
    user_credential_filepath=".oauth/credentials.json",
)
print(cred.token)

# TODO: should merge SA crednetial, user credential together? or they should be
# in different place


# using adc json works!
# c = Credentials.from_authorized_user_file(
#     filename=Path.home() / ".config/gcloud/application_default_credentials.json"
# )

# c.refresh(Request())
# print(c.token)
