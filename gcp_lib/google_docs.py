from googleapiclient.discovery import build
import google.auth


class GoogleDocs:
    def __init__(self, document_id: str = None):
        """if document_id provided, all other doc relation method will be point to it by default

        Args:
            document_id (str, optional): Google Docs ID (in url). Defaults to None.

        Ref:
            https://googleapis.github.io/google-api-python-client/docs/dyn/docs_v1.html
        """
        self.document_id = document_id

        self.doc = self._get_doc_client()

    def _get_doc_client(self):
        """return a client to interact with google docs

        Returns:
            _type_: _description_
        """
        SCOPES = [
            "https://www.googleapis.com/auth/documents",
            "https://www.googleapis.com/auth/drive",
        ]

        creds, _ = google.auth.default(scopes=SCOPES)
        service = build("docs", "v1")  # , credentials=creds)
        return service

    def _get_doc(self, document_id: str = None):
        """get document object

        Args:
            document_id (str, optional): _description_. Defaults to None.

        Returns:
            _type_: _description_
        """
        document_id = document_id or self.document_id
        return self.doc.documents().get(documentId=document_id).execute()

    def get_doc_url(self, doc_id):
        return f"https://docs.google.com/document/d/{doc_id}"

    def create_doc(self, title: str = None) -> dict:
        """create new googel doc

        Args:
            title (str, optional): title of the docs. Defaults to None.

        Returns:
            dict: document instance for this newly created doc
        """
        return self.doc.documents().create(body={"title": title}).execute()

    def insert_text_to_doc(self, text: str, document_id: str = None):
        """insert text to a doc

        Args:
            text (str): _description_
            document_id (str, optional): _description_. Defaults to None.

        Returns:
            _type_: _description_
        """
        document_id = document_id or self.document_id
        requests = [
            {
                "insertText": {
                    "location": {
                        "index": 1,
                    },
                    "text": text,
                }
            }
        ]
        return (
            self.doc.documents()
            .batchUpdate(documentId=document_id, body={"requests": requests})
            .execute()
        )


if __name__ == "__main__":
    # from google_drive import GoogleDrive
    import os

    # from rich import print

    # os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "key/sa_key.json"
    # doc = GoogleDocs()
    # d = doc._get_doc(document_id="1yW7n1VF1AjaaPgO_gksNq0DLJIhd9SVpVXfWhaFpR6s")
    # import json

    # print(json.dumps(d))

    from googleapiclient.discovery import build

    doc = build("docs", "v1")
    document_id = "1yW7n1VF1AjaaPgO_gksNq0DLJIhd9SVpVXfWhaFpR6s"
    content = doc.documents().get(documentId=document_id).execute()
    import json

    out = json.dumps(content)
    print(out)
    # print(type(d))
    # = doc.create_doc("abc")

    # drive = gdrive()
    # DOCUMENT_ID = "12NFtspNtF0w2_aNzPv6XtzvN1lCEV9I61DexnvkBcNQ"
    # # result = doc._get_doc()
    # result = doc.create_doc("abc")
    # drive.grant_permission_to_user(
    #     result["documentId"], role="writer", user_email="fung.yeung@veolia.com"
    # )
    # print(result["documentId"])
