from __future__ import print_function

from pathlib import Path

from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError


def get_user_credential(
    user_credential_json_path: str,
    oauth_scopes: list,
    oauth_local_port: int = 1234,
    token_path: str | None = None,
) -> Credentials:
    """https://developers.google.com/identity/protocols/oauth2/scopes"""
    creds = None
    # The file token.json stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if token_path is None:
        Path(".oauth").mkdir(exist_ok=True)
        token_path = ".oauth/token.json"

    if Path(token_path).exists():
        creds = Credentials.from_authorized_user_file(
            filename=token_path, scopes=oauth_scopes
        )
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                user_credential_json_path, oauth_scopes
            )
            creds = flow.run_local_server(port=oauth_local_port)
        # Save the credentials for the next run
        user_credential_json_path = ".oauth/token.json"

        with open(user_credential_json_path, "w") as token:
            token.write(creds.to_json())

    return creds


if __name__ == "__main__":
    cred = get_user_credential(
        user_credential_json_path=Path.home()
        / ".config/gcloud/application_default_credentials.json",
        oauth_scopes=["https://www.googleapis.com/auth/drive"],
    )

    # from gdrive import GoogleDrive
    from googleapiclient.discovery import build
    from rich import print

    d = build("drive", "v3", credentials=cred)
    output = d.files().list(fields="files(id, name, mimeType)").execute()
    print(output["files"])
